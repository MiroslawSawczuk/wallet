import React from 'react';
import MainNavigation from './src/navigation/MainNavigation';
import { Provider } from 'react-redux';
import { persistor, store } from './src/store/rootStore';
import { PersistGate } from 'redux-persist/integration/react';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <MainNavigation />
      </PersistGate>
    </Provider>
  );
};

export default App;
