import { ethers } from 'ethers';

const PROJECT_ID = '4ca2f004842d4a6686fa2d523ea684b9';
// const ETH_TESTNET = 'https://sepolia.infura.io/v3/';
const PROVIDER_NAME = 'sepolia';

export class EthClass {
  private static _instance: EthClass;
  eth: ethers.InfuraProvider = new ethers.InfuraProvider(PROVIDER_NAME, PROJECT_ID);

  public getEth = () => {
    return this.eth;
  };

  public initializeEth = () => {
    this.eth = new ethers.InfuraProvider(PROVIDER_NAME, PROJECT_ID);
  };

  public static getInstance() {
    if (EthClass._instance == null) {
      EthClass._instance = new EthClass();
      if (this._instance.eth == null) {
        this._instance.initializeEth();
      }
    }
    return this._instance;
  }
  constructor() {
    if (EthClass._instance) {
      throw new Error('Error: Instantiation failed: Use EthClass.getInstance() instead of new.');
    }
  }
}
