import { Web3Class } from './Web3Class';

export const getBalance = async (address: string) => {
  try {
    const web3 = Web3Class.getInstance().getWeb3();

    const ethBalance = await web3.eth.getBalance(address);
    const weiBalance = web3.utils.fromWei(ethBalance, 'ether');

    return weiBalance;
  } catch (ex) {
    console.log(`getBalance ${ex}`);
    return '0';
  }
};

export const sendCoin = async (addressFrom: string, addressTo: string, value: number, privateKey: string) => {
  try {
    const web3 = Web3Class.getInstance().getWeb3();
    const gasPrice = await web3.eth.getGasPrice();
    const gasLimit = 21000;
    var nonce = await web3.eth.getTransactionCount(addressFrom);
    const tx = {
      from: addressFrom,
      to: addressTo,
      value: web3.utils.toWei(value.toString(), 'ether'),
      gasPrice,
      gasLimit,
      nonce,
    };

    const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey);
    const txHash = await web3.eth.sendSignedTransaction(signedTx.rawTransaction || '');

    console.log('Transaction sent', txHash);
    return true;
  } catch (ex) {
    console.log(`sendCoin: ${ex}`);
    return false;
  }
};
