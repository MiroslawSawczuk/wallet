import Web3 from 'web3';
import { RegisteredSubscription } from 'web3/lib/commonjs/eth.exports';

const PROJECT_ID = '4ca2f004842d4a6686fa2d523ea684b9';
const ETH_TESTNET = 'https://sepolia.infura.io/v3/';
const provider = `${ETH_TESTNET}${PROJECT_ID}`;

export class Web3Class {
  private static _instance: Web3Class;
  web3: Web3<RegisteredSubscription> = new Web3(new Web3.providers.HttpProvider(provider));

  public getWeb3 = (): Web3 => {
    return this.web3;
  };

  public initializeWeb3 = () => {
    this.web3 = new Web3(new Web3.providers.HttpProvider(provider));
  };

  public static getInstance(): Web3Class {
    if (Web3Class._instance == null) {
      Web3Class._instance = new Web3Class();
      if (this._instance.web3 == null) {
        this._instance.initializeWeb3();
      }
    }
    return this._instance;
  }
  constructor() {
    if (Web3Class._instance) {
      throw new Error('Error: Instantiation failed: Use Web3Class.getInstance() instead of new.');
    }
  }
}
