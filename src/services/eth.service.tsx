import { HDNodeWallet, ethers } from 'ethers';
import { EthClass } from './ETHClass';

// export const initializeEthService = () => {
//   try {
//     EthClass.getInstance().getEth();
//   } catch (ex) {
//     console.log(`initializeEthService ${ex}`);
//   }
// };

export const initializeEthService = (): Promise<void> => {
  return new Promise(async (resolve, reject) => {
    try {
      await EthClass.getInstance().getEth();
      resolve();
    } catch (ex) {
      console.error(`initializeEthService ${ex}`);
      reject(ex);
    }
  });
};

// export const createRandomWallet = async () => {
//   try {
//     const ethProvider = EthClass.getInstance().eth;
//     const wallet = ethers.Wallet.createRandom(ethProvider);

//     return wallet;
//   } catch (ex) {
//     console.log(`createRandomWallet ${ex}`);
//   }
// };

export const createRandomWallet = (): Promise<HDNodeWallet> => {
  return new Promise((resolve, reject) => {
    try {
      const ethProvider = EthClass.getInstance().eth;
      const wallet = ethers.Wallet.createRandom(ethProvider);

      resolve(wallet);
    } catch (ex) {
      console.error(`createRandomWallet ${ex}`);
      reject(ex);
    }
  });
};

export const createWalletFromSeeds = async (phrases: string) => {
  try {
    const ethProvider = EthClass.getInstance().getEth();
    const wallet = ethers.Wallet.fromPhrase(phrases, ethProvider);
    return wallet;
  } catch (ex) {
    console.log(`createWalletFromSeeds ${ex}`);
  }
};

export const createWalletFromPrivateKey = async (privateKey: string) => {
  try {
    const ethProvider = EthClass.getInstance().getEth();
    const wallet = new ethers.Wallet(privateKey, ethProvider);

    return wallet;
  } catch (ex) {
    console.log(`createWalletFromPrivateKey ${ex}`);
  }
};

export const getCoinBalance = async (address: string) => {
  try {
    const ethProvider = EthClass.getInstance().getEth();
    const gweiBalance = await ethProvider.getBalance(address);
    const ethBalance = ethers.formatEther(gweiBalance);

    return ethBalance;
  } catch (ex) {
    console.log(`getBalance ${ex}`);
  }
};

export const sendToken_ERC20 = async (
  privateKey: string,
  addressTo: string,
  tokenAddress: string,
  amount: string,
  tokenAbi: any,
) => {
  try {
    const wallet = await createWalletFromPrivateKey(privateKey);

    if (wallet) {
      const contract = new ethers.Contract(tokenAddress, tokenAbi, wallet);
      const decimals = await contract.decimals();
      const amountGwei = ethers.parseUnits(amount, decimals);

      const tx = await contract.transfer(addressTo, amountGwei);
      await tx.wait();

      console.log('Transaction Hash:', tx.hash);
      return true;
    }
    return false;
  } catch (ex) {
    console.log(`sendToken_ERC20 ${ex}`);
    return false;
  }
};

export const getTokenBalance = async (
  address: string,
  tokenContractAbi: any,
  privateKey: string,
  tokenAddress: string,
) => {
  try {
    const wallet = await createWalletFromPrivateKey(privateKey);
    if (wallet) {
      const tokenContract = new ethers.Contract(tokenAddress, tokenContractAbi, wallet);
      const balance = await tokenContract.balanceOf(address);
      const balanceFormatted = ethers.formatUnits(balance, 18);

      return balanceFormatted;
    }
  } catch (ex) {
    console.log(`getBalanceAddressToken ${ex}`);
    return '0';
  }
};
