export const isNumeric = (value: any) => {
  return value.trim() && !Number.isNaN(value);
};
