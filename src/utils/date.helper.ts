export enum DateFormats {
  'DDMMYY' = 'DD / MM / YYYY',
  'DDMMYYHHMM' = 'DD / MM / YYYY HH:MM',
}

const formatDateAsDDMMYY = (date: Date) => {
  const [year, month, day] = [date.getFullYear(), date.getMonth(), date.getDate()];

  const oneDigitMonthNUmber = 10;
  const monthFormatted = month + 1 < oneDigitMonthNUmber ? `0${month + 1}` : month + 1;

  return `${day} / ${monthFormatted} / ${year}`;
};
const formatDateAsDDMMYYHHMM = (date: Date) => {
  const [year, month, day, hours, minutes] = [
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    date.getHours(),
    date.getMinutes(),
  ];

  const oneDigitMonthNUmber = 10;
  const monthFormatted = month + 1 < oneDigitMonthNUmber ? `0${month + 1}` : month + 1;

  return `${day}.${monthFormatted}.${year} ${hours}: ${minutes}`;
};

export const getFormattedDate = (date: Date, format: DateFormats = DateFormats.DDMMYY) => {
  if (!(date instanceof Date && !isNaN(date.getTime()))) {
    return '';
  }

  switch (format) {
    case DateFormats.DDMMYY:
      return formatDateAsDDMMYY(date);
    case DateFormats.DDMMYYHHMM:
      return formatDateAsDDMMYYHHMM(date);

    default:
      return formatDateAsDDMMYY(date);
  }
};
