import { ethers } from 'ethers';

export const formatToEth = (gwei: string) => {
  return Number(ethers.formatUnits(BigInt(gwei)));
};
