export type QRCodePayload = {
  address: string;
  amount: string;
};
