export enum CoinType {
  Coin,
  Token,
}

export enum TokenStandard {
  ERC20 = 'ERC20',
}
