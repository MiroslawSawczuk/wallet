import { RouteProp } from '@react-navigation/native';
import { HDNodeWallet } from 'ethers';

export enum StackName {
  ONBOARDING_STACK = 'ONBOARDING_STACK',
  HOME_STACK = 'HOME_STACK',
}

export enum ScreenName {
  WELCOME = 'WELCOME',
  ADD_EXISTING_WALLET = 'ADD_EXISTING_WALLET',
  SELECT_NETWORK = 'SELECT_NETWORK',
  RESTORE_WALLET = 'RESTORE_WALLET',
  CREATE_PIN = 'CREATE_PIN',
  BACKUP = 'BACKUP',
  SECRET_PHRASE = 'SECRET_PHRASE',
  SECRET_PHRASE_REPEAT = 'SECRET_PHRASE_REPEAT',

  PORTFOLIO = 'PORTFOLIO',
  ASSET_OVERVIEW = 'ASSET_OVERVIEW',
  SEND = 'SEND',
  RECEIVE = 'RECEIVE',
}

export type ScreenParamListType = {
  WELCOME?: undefined;
  ADD_EXISTING_WALLET?: undefined;
  SELECT_NETWORK?: undefined;
  RESTORE_WALLET: { networkName: string };
  CREATE_PIN?: undefined;
  BACKUP?: undefined;
  SECRET_PHRASE?: undefined;
  SECRET_PHRASE_REPEAT: {
    wallet: HDNodeWallet;
  };

  PORTFOLIO?: undefined;
  ASSET_OVERVIEW?: undefined;
  SEND?: undefined;
  RECEIVE?: undefined;
};

export type WelcomeRouteProps = RouteProp<ScreenParamListType, ScreenName.WELCOME>;
export type AddExistingWalletRouteProps = RouteProp<ScreenParamListType, ScreenName.ADD_EXISTING_WALLET>;
export type SelectNetworkRouteProps = RouteProp<ScreenParamListType, ScreenName.SELECT_NETWORK>;
export type RestoreWalletRouteProps = RouteProp<ScreenParamListType, ScreenName.RESTORE_WALLET>;
export type CreatePinRouteProps = RouteProp<ScreenParamListType, ScreenName.CREATE_PIN>;
export type BackupRouteProps = RouteProp<ScreenParamListType, ScreenName.BACKUP>;
export type SecretPhraseRouteProps = RouteProp<ScreenParamListType, ScreenName.SECRET_PHRASE>;
export type SecretPhraseConfirmRouteProps = RouteProp<ScreenParamListType, ScreenName.SECRET_PHRASE_REPEAT>;

export type PortfolioRouteProps = RouteProp<ScreenParamListType, ScreenName.PORTFOLIO>;
export type AssetOverviewRouteProps = RouteProp<ScreenParamListType, ScreenName.ASSET_OVERVIEW>;
export type SendRouteProps = RouteProp<ScreenParamListType, ScreenName.SEND>;
export type ReceiveRouteProps = RouteProp<ScreenParamListType, ScreenName.RECEIVE>;
