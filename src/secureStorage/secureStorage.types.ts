export const PUBLIC_KEY = 'PUBLIC_KEY';
export const PRIVATE_KEY = 'PRIVATE_KEY';
export const ETH_ADDRESS = 'ETH_ADDRESS';
export const PIN = 'PIN';
