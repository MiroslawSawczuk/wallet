import EncryptedStorage from 'react-native-encrypted-storage';

export const setItem = async (key: string, value: string) => {
  try {
    return await EncryptedStorage.setItem(key, value);
  } catch (ex) {
    console.log(`Error: ${ex}`);
    return null;
  }
};

export const setObject = async (key: string, value: object) => {
  try {
    return await EncryptedStorage.setItem(key, JSON.stringify(value));
  } catch (ex) {
    console.log(`Error: ${ex}`);
    return null;
  }
};

export const getObject = async (key: string) => {
  try {
    const result = await EncryptedStorage.getItem(key);
    if (result) {
      return JSON.parse(result);
    } else {
      return result;
    }
  } catch (ex) {
    console.log(`Error: ${ex}`);
    return null;
  }
};
export const getItem = async (key: string) => {
  try {
    return await EncryptedStorage.getItem(key);
  } catch (ex) {
    console.log(`Error: ${ex}`);
    return null;
  }
};

export const removeItem = async (key: string) => {
  try {
    return await EncryptedStorage.removeItem(key);
  } catch (ex) {
    console.log(`Error: ${ex}`);
    return null;
  }
};
