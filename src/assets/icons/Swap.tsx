import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { OneIconProps } from '../../components/Icon/Icon';

export const Swap = ({ color = 'black', size = 24, ...props }: OneIconProps) => {
  return (
    <Svg width={size} height={size} viewBox="0 0 24 24" fill="none" {...props}>
      <Path
        d="M8 3.5v13m0-13L3.5 7.833M8 3.5l4.5 4.333M17 20.5v-13m0 13l4.5-4.333M17 20.5l-4.5-4.333"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};
