import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { OneIconProps } from '../../components/Icon/Icon';

export const Copy = ({ color = 'black', size = 24, ...props }: OneIconProps) => {
  return (
    <Svg width={size} height={size} viewBox="0 0 24 24" fill="none" {...props}>
      <Path
        d="M19.53 8L14 2.47a.75.75 0 00-.53-.22H11A2.75 2.75 0 008.25 5v1.25H7A2.75 2.75 0 004.25 9v10A2.75 2.75 0 007 21.75h7A2.75 2.75 0 0016.75 19v-1.25H17A2.75 2.75 0 0019.75 15V8.5a.75.75 0 00-.22-.5zm-5.28-3.19l2.94 2.94h-2.94V4.81zm1 14.19A1.25 1.25 0 0114 20.25H7A1.25 1.25 0 015.75 19V9A1.25 1.25 0 017 7.75h1.25V15A2.75 2.75 0 0011 17.75h4.25V19zM17 16.25h-6A1.25 1.25 0 019.75 15V5A1.25 1.25 0 0111 3.75h1.75V8.5a.76.76 0 00.75.75h4.75V15A1.25 1.25 0 0117 16.25z"
        fill={color}
      />
    </Svg>
  );
};
