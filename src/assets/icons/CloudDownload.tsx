import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { OneIconProps } from '../../components/Icon/Icon';

export const CloudDownload = ({ color = 'black', size = 24, ...props }: OneIconProps) => {
  return (
    <Svg width={size} height={size} viewBox="0 0 24 24" fill="none" {...props}>
      <Path
        d="M5.256 16A5.33 5.33 0 013 11.65C3 9.2 4.8 6.936 7.5 6.5 8.347 4.486 10.351 3 12.69 3c2.994 0 5.442 2.323 5.61 5.25 1.59.695 2.7 2.4 2.7 4.247a4.5 4.5 0 01-2 3.745M12 21V11m0 10l-3-3m3 3l3-3"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};
