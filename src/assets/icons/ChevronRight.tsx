import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { OneIconProps } from '../../components/Icon/Icon';

export const ChevronRight = ({ color = 'black', size = 24, ...props }: OneIconProps) => {
  return (
    <Svg width={size} height={size} viewBox="0 0 24 24" fill="none" {...props}>
      <Path fill={'none'} />
      <Path d="M9.5 7l5 5-5 5" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </Svg>
  );
};
