import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { OneIconProps } from '../../components/Icon/Icon';

export const Send = ({ color = 'black', size = 24, ...props }: OneIconProps) => {
  return (
    <Svg width={size} height={size} viewBox="0 0 24 24" fill="none" {...props}>
      <Path
        d="M10 14l3 7 7-17-17 7 3.5 1.5"
        stroke={color}
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};
