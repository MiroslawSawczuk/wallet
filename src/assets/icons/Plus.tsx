import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { OneIconProps } from '../../components/Icon/Icon';

export const Plus = ({ color = 'black', size = 24, ...props }: OneIconProps) => {
  return (
    <Svg width={size} height={size} viewBox="0 0 24 24" fill="none" {...props}>
      <Path fill={color} />
      <Path d="M12 6v12M6 12h12" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </Svg>
  );
};
