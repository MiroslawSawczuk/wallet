import { ArrowDown } from './ArrowDown';
import { ArrowLeft } from './ArrowLeft';
import { Camera } from './Camera';
import { ChevronRight } from './ChevronRight';
import { CloudDownload } from './CloudDownload';
import { Copy } from './Copy';
import { Eye } from './Eye';
import { Plus } from './Plus';
import { QrCode } from './QrCode';
import { Receive } from './Receive';
import { Send } from './Send';
import { Swap } from './Swap';

export const iconNames = {
  plus: Plus,
  arrowDown: ArrowDown,
  arrowLeft: ArrowLeft,
  chevronRight: ChevronRight,
  cloudDownload: CloudDownload,
  eye: Eye,
  copy: Copy,
  send: Send,
  receive: Receive,
  swap: Swap,
  camera: Camera,
  qrCode: QrCode,
} as const;

export default {
  Plus: Plus,
  ArrowDown: ArrowDown,
  ArrowLeft: ArrowLeft,
  ChevronRight: ChevronRight,
  CloudDownload: CloudDownload,
  Eye: Eye,
  Copy: Copy,
  Send: Send,
  Receive: Receive,
  Swap: Swap,
  Camera: Camera,
  QrCode: QrCode,
};

export type IconName = keyof typeof iconNames;
