import * as React from 'react';
import Svg, { Path } from 'react-native-svg';
import { OneIconProps } from '../../components/Icon/Icon';

export const ArrowLeft = ({ color = 'black', size = 24, ...props }: OneIconProps) => {
  return (
    <Svg width={size} height={size} viewBox="0 0 24 24" fill="none" {...props}>
      <Path
        d="M10.767 7.543a.75.75 0 10-1.034-1.086l1.034 1.086zm-6.284 3.914a.75.75 0 101.034 1.086l-1.034-1.086zm1.034 0a.75.75 0 10-1.034 1.086l1.034-1.086zm4.216 6.086a.75.75 0 101.034-1.086l-1.034 1.086zM5 11.25a.75.75 0 000 1.5v-1.5zm14 1.5a.75.75 0 000-1.5v1.5zM9.733 6.457l-5.25 5 1.034 1.086 5.25-5-1.034-1.086zm-5.25 6.086l5.25 5 1.034-1.086-5.25-5-1.034 1.086zM5 12.75h14v-1.5H5v1.5z"
        fill={color}
      />
    </Svg>
  );
};
