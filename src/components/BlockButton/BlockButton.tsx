import React, { FC } from 'react';
import { StyleProp, Text, TouchableOpacity, TouchableOpacityProps, View, ViewStyle } from 'react-native';
import { useBlockButtonStyles } from './BlockButton.styles';

type Props = TouchableOpacityProps & {
  title: string;
  description?: string;
  leftIcon?: JSX.Element;
  rightIcon?: JSX.Element;
  containerStyle?: StyleProp<ViewStyle>;
  disabled?: boolean;
};
export const BlockButton: FC<Props> = ({
  title,
  description,
  leftIcon,
  rightIcon,
  containerStyle,
  disabled = false,
  ...props
}) => {
  const styles = useBlockButtonStyles();

  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[styles.container, disabled && styles.disabled, containerStyle]}
      {...props}
      disabled={disabled}
    >
      {!!leftIcon && (
        <View style={styles.iconLeftContainer}>
          <View style={styles.circle}>{leftIcon}</View>
        </View>
      )}
      <View style={styles.content}>
        <Text style={styles.title}>{title}</Text>

        {!!description && <Text style={styles.description}>{description}</Text>}
      </View>

      {!!rightIcon && <View style={styles.iconRightContainer}>{rightIcon}</View>}
    </TouchableOpacity>
  );
};
