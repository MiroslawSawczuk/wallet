import { StyleSheet } from 'react-native';
import { color, fontSize, radius } from '../../utils/colors';

export const useBlockButtonStyles = () => {
  return StyleSheet.create({
    container: {
      height: 70,
      flexDirection: 'row',
      backgroundColor: color.grey9,
      borderRadius: radius.medium,
      paddingVertical: 10,
      paddingHorizontal: 20,
    },
    disabled: {
      opacity: 0.6,
    },
    iconLeftContainer: {
      width: 50,
      justifyContent: 'center',
      alignItems: 'flex-start',
    },
    iconRightContainer: {
      width: 50,
      justifyContent: 'center',
      alignItems: 'flex-end',
    },
    circle: {
      borderRadius: radius.xLarge,
      width: 30,
      height: 30,
      backgroundColor: color.blue9,
      justifyContent: 'center',
      alignItems: 'center',
    },
    content: {
      padding: 5,
      justifyContent: 'center',
      flex: 1,
    },
    title: {
      marginBottom: 5,
      fontSize: fontSize.medium,
      color: color.grey1,
      fontWeight: '500',
    },
    description: {
      fontSize: fontSize.xSmall,
      color: color.grey2,
    },
  });
};
