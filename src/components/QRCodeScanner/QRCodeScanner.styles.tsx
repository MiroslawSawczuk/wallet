import { StyleSheet, useWindowDimensions } from 'react-native';

export const useQRCodeScannerStyles = () => {
  const { width, height } = useWindowDimensions();
  return StyleSheet.create({
    camera: {
      width: width,
      height: height,
    },
  });
};
