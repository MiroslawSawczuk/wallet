import React, { FC, useEffect } from 'react';
import { View, Text } from 'react-native';
import { Camera, useCameraDevice, useCameraPermission, useCodeScanner } from 'react-native-vision-camera';
import { useQRCodeScannerStyles } from './QRCodeScanner.styles';

type Props = {
  onCodeScanned: (address: string, amount: string) => void;
};

export const QRCodeScanner: FC<Props> = ({ onCodeScanned }) => {
  const styles = useQRCodeScannerStyles();
  const { hasPermission, requestPermission } = useCameraPermission();
  const device = useCameraDevice('back');

  const codeScanner = useCodeScanner({
    codeTypes: ['qr', 'ean-13'],
    onCodeScanned: codes => {
      const { address, amount } = JSON.parse(codes[0]?.value);
      onCodeScanned(address, amount);
    },
  });

  useEffect(() => {
    if (!hasPermission) {
      requestPermission();
    }
  }, []);

  return (
    <View>
      {!hasPermission || device == null ? (
        <Text>no camera permission camera</Text>
      ) : (
        <Camera style={styles.camera} device={device} isActive={true} codeScanner={codeScanner} />
      )}
    </View>
  );
};
