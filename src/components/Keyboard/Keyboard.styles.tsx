import { useWindowDimensions, StyleSheet } from 'react-native';
import { space, fontSize, color, radius } from '../../utils/colors';

export const useKeyboardStyles = () => {
  const { width } = useWindowDimensions();

  return StyleSheet.create({
    keyboard: {
      paddingHorizontal: space.large,
    },
    keyContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    row: {
      flexDirection: 'row',
      height: width / 5,
    },
    keyLabel: {
      fontSize: fontSize.xxLarge,
      color: color.grey3,
      fontWeight: '600',
    },
    hitSlop: {
      top: 25,
      bottom: 25,
      left: 25,
      right: 25,
    },
    key: {
      borderRadius: radius.full,
      width: 40,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
};
