import React, { FC } from 'react';
import { Text, TouchableHighlight, View } from 'react-native';
import { useKeyboardStyles } from './Keyboard.styles';
import { color } from '../../utils/colors';

const keyboard = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
  ['', 0, 'remove'],
];

type Props = {
  onPressKey: (key: string | number) => void;
};
export const Keyboard: FC<Props> = ({ onPressKey }) => {
  const styles = useKeyboardStyles();

  return (
    <View style={styles.keyboard}>
      {keyboard.map((row, rowIndex) => (
        <View key={rowIndex} style={styles.row}>
          {row.map((key, keyIndex) => (
            <View key={keyIndex} style={styles.keyContainer}>
              <TouchableHighlight
                onPress={() => onPressKey(key)}
                hitSlop={styles.hitSlop}
                style={styles.key}
                activeOpacity={0.8}
                underlayColor={color.blue10}
              >
                <Text style={styles.keyLabel}>{key === 'remove' ? '<-' : key}</Text>
              </TouchableHighlight>
            </View>
          ))}
        </View>
      ))}
    </View>
  );
};
