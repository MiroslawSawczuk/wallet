import { StyleSheet } from 'react-native';
import { color, space, radius } from '../../utils/colors';

export const usePINBoxesStyles = () => {
  return StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: space.xLarge,
    },
    pinSingleContainer: {
      borderWidth: 1,
      borderColor: color.grey5,
      height: 40,
      width: 35,
      borderRadius: radius.xxSmall,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
};
