import React, { FC } from 'react';
import { Text, View } from 'react-native';
import { usePINBoxesStyles } from './PINBoxes.styles';

type Props = {
  pin: string[];
};
export const PINBoxes: FC<Props> = ({ pin }) => {
  const styles = usePINBoxesStyles();

  return (
    <View style={styles.container}>
      {pin?.map((value, index) => (
        <View key={index} style={styles.pinSingleContainer}>
          <Text>{value}</Text>
        </View>
      ))}
    </View>
  );
};
