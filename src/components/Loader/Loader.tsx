import React from 'react';
import { ActivityIndicator, Modal, StyleSheet, View } from 'react-native';

const Loader = () => {
  return (
    <Modal animationType="fade" transparent={true} visible={true}>
      <View style={styles.container}>
        <ActivityIndicator />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
});

export default Loader;
