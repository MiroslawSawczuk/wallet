import { StyleSheet } from 'react-native';
import { color, fontSize, space } from '../../utils/colors';

export const useBaseInputStyles = () => {
  return StyleSheet.create({
    container: {
      borderWidth: 2,
      borderColor: color.grey9,
      minHeight: 55,
      padding: space.small,
      fontSize: fontSize.xLarge,
    },
    label: {
      fontSize: fontSize.xMedium,
      color: color.grey5,
      fontWeight: '600',
      marginBottom: space.xxSmall,
    },
  });
};
