import React, { FC } from 'react';
import { StyleProp, Text, TextInput, TextInputProps, View, ViewStyle } from 'react-native';
import { useBaseInputStyles } from './BaseInput.styles';

type Props = TextInputProps & {
  label: string;
  containerStyles?: StyleProp<ViewStyle>;
  wrapperStyles?: StyleProp<ViewStyle>;
};
export const BaseInput: FC<Props> = ({ label, containerStyles, wrapperStyles, ...textInputProps }) => {
  const styles = useBaseInputStyles();

  return (
    <View style={wrapperStyles}>
      <Text style={styles.label}>{label}</Text>
      <TextInput style={[styles.container, containerStyles]} {...textInputProps} />
    </View>
  );
};
