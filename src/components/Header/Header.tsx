import React, { FC } from 'react';
import { IconName } from '../../assets/icons';
import { useHeaderStyles } from './Header.styles';
import { Text, TouchableOpacity, View } from 'react-native';
import { Icon } from '../Icon/Icon';

type Props = {
  title: string;
  leftIconName?: IconName;
  rightIconName?: IconName;
  onPressLeftIcon?: () => void;
  onPressRightIcon?: () => void;
};
export const Header: FC<Props> = ({ title, leftIconName, rightIconName, onPressLeftIcon, onPressRightIcon }) => {
  const styles = useHeaderStyles();

  return (
    <View style={styles.container}>
      <View style={styles.iconLeftContainer}>
        {!!leftIconName && (
          <TouchableOpacity onPress={onPressLeftIcon}>
            <Icon name={leftIconName} />
          </TouchableOpacity>
        )}
      </View>

      <View style={styles.titleContainer}>
        <Text style={styles.title}> {title}</Text>
      </View>

      <View style={styles.iconRightContainer}>
        {!!rightIconName && (
          <TouchableOpacity onPress={onPressRightIcon}>
            <Icon name={rightIconName} />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};
