import { StyleSheet } from 'react-native';

export const useHeaderStyles = () => {
  return StyleSheet.create({
    container: {
      height: 56,
      flexDirection: 'row',
      paddingVertical: 10,
      paddingHorizontal: 15,
    },
    iconLeftContainer: {
      justifyContent: 'center',
      alignItems: 'flex-start',
      width: 50,
    },
    iconRightContainer: {
      justifyContent: 'center',
      alignItems: 'flex-end',
      width: 50,
    },
    titleContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    title: {
      fontSize: 20,
      fontWeight: '500',
    },
  });
};
