import React, { FC } from 'react';
import { StyleProp, Text, TextStyle, TouchableOpacity, TouchableOpacityProps, ViewStyle } from 'react-native';
import { useBaseButtonStyles } from './BaseButton.styles';

type Props = TouchableOpacityProps & {
  title: string;
  labelStyle?: StyleProp<TextStyle>;
  containerStyle?: StyleProp<ViewStyle>;
};
export const BaseButton: FC<Props> = ({ title, labelStyle, containerStyle, ...props }) => {
  const styles = useBaseButtonStyles(props.disabled);

  return (
    <TouchableOpacity onPress={props.onPress} style={[styles.container, containerStyle]} {...props}>
      <Text style={[styles.label, labelStyle]}>{title}</Text>
    </TouchableOpacity>
  );
};
