import { StyleSheet } from 'react-native';
import { color, fontSize, space, radius } from '../../utils/colors';

export const useBaseButtonStyles = (disabled = false) => {
  return StyleSheet.create({
    container: {
      height: 40,
      backgroundColor: color.grey9,
      borderRadius: radius.large,
      padding: space.xxSmall,
      justifyContent: 'center',
      alignItems: 'center',
      opacity: disabled ? 0.6 : 1,
    },
    label: {
      fontSize: fontSize.medium,
      color: color.blue5,
      fontWeight: '500',
    },
  });
};
