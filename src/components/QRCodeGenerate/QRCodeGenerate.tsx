import React, { FC } from 'react';
import QRCode from 'react-native-qrcode-svg';
import { QRCodePayload } from '../../@types/receive.types';
import { StyleProp, View, ViewStyle } from 'react-native';

type Props = {
  data: QRCodePayload;
  size: number;
  containerStyle?: StyleProp<ViewStyle>;
};

const QRCodeGenerate: FC<Props> = ({ data, size, containerStyle }) => {
  return (
    <View style={containerStyle}>
      <QRCode value={JSON.stringify(data)} size={size} logoBackgroundColor="transparent" />
    </View>
  );
};

export default QRCodeGenerate;
