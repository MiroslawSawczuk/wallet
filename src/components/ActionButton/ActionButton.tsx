import React, { FC } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { Icon } from '../Icon/Icon';
import { IconName } from '../../assets/icons';
import { useActionButtonStyles } from './ActionButton.styles';
import { color } from '../../utils/colors';

type Props = {
  title: string;
  iconName: IconName;
  disabled?: boolean;
  onPress: () => void;
};
export const ActionButton: FC<Props> = ({ title, iconName, disabled = false, onPress }) => {
  const styles = useActionButtonStyles(disabled);
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button} onPress={onPress} disabled={disabled}>
        <Icon name={iconName} size={20} color={disabled ? color.grey7 : undefined} />
      </TouchableOpacity>

      <Text style={styles.title}>{title}</Text>
    </View>
  );
};
