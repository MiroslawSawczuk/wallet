import { StyleSheet } from 'react-native';
import { color, fontSize, radius, space } from '../../utils/colors';

export const useActionButtonStyles = (disabled: boolean) => {
  return StyleSheet.create({
    container: {
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    button: {
      borderWidth: 1,
      borderColor: disabled ? color.grey7 : color.grey5,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: radius.full,
      width: 40,
      height: 40,
      marginBottom: space.xSmall,
    },
    title: {
      color: disabled ? color.grey7 : color.grey1,
      fontSize: fontSize.medium,
      fontWeight: '600',
    },
  });
};
