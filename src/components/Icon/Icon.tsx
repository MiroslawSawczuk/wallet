import { FC, createElement } from 'react';
import { TextStyle, ViewStyle } from 'react-native';
import { SvgProps } from 'react-native-svg';
import { IconName, iconNames } from '../../assets/icons';

export type IconProps = SvgProps & {
  color?: TextStyle['color'];
  secondColor?: TextStyle['color'];
  style?: ViewStyle;
  size?: number;
  name: IconName;
};

export type OneIconProps = Omit<IconProps, 'name'>;

export const Icon: FC<IconProps> = ({ name = 'plus', ...props }) => createElement(iconNames[name], { ...props });
