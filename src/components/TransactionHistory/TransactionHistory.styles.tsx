import { StyleSheet } from 'react-native';
import { color, fontSize, radius, space } from '../../utils/colors';

export const useTransactionHistoryStyles = () => {
  return StyleSheet.create({
    container: {
      flexDirection: 'row',

      height: 40,
      backgroundColor: color.blue10,
      borderRadius: radius.medium,
      paddingVertical: 10,
      paddingHorizontal: 20,
      marginBottom: space.small,
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    content: {
      fontSize: fontSize.medium,
      fontWeight: '400',
      color: color.grey1,
    },
    positive: {
      fontSize: fontSize.medium,
      fontWeight: '400',
      color: color.green2,
    },
    negative: {
      fontSize: fontSize.medium,
      fontWeight: '400',
      color: color.red2,
    },
    title: {
      color: color.grey1,
      fontSize: fontSize.medium,
      fontWeight: '600',
      marginTop: space.large,
      marginBottom: space.xSmall,
    },
  });
};
