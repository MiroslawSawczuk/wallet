import React, { FC, useEffect } from 'react';
import { Text, View } from 'react-native';
import { useTransactionHistoryStyles } from './TransactionHistory.styles';
import { useGetTransactionsHistoryByAddressQuery } from '../../store/etherscanSepolia/etherscanSepolia.api';
import { DateFormats, getFormattedDate } from '../../utils/date.helper';
import { formatToEth } from '../../utils/ethers.helpers';

type Props = {
  address: string;
  contractAddress?: string;
  ticker: string;
  balance: number;
};
export const TransactionHistory: FC<Props> = ({ address, ticker, balance, contractAddress }) => {
  const { data, refetch } = useGetTransactionsHistoryByAddressQuery({
    address,
    contractAddress,
  });
  const styles = useTransactionHistoryStyles();

  useEffect(() => {
    refetch();
  }, [balance]);

  const getPrefix = (targetAddress: string) => {
    return targetAddress.toLowerCase() === address.toLowerCase() ? '+' : '-';
  };

  return (
    <>
      <Text style={styles.title}>Transaction History</Text>
      {data?.result?.map((tx, index) => (
        <View key={index} style={styles.container}>
          <Text style={styles.content}>
            {address === tx.to ? 'Received' : 'Send'} at{'   '}
            {getFormattedDate(new Date(Number(tx.timeStamp)), DateFormats.DDMMYYHHMM)}
          </Text>

          <Text style={address.toLowerCase() === tx.to.toLowerCase() ? styles.positive : styles.negative}>
            {getPrefix(tx.to)} {formatToEth(tx.value)} {ticker}
          </Text>
        </View>
      ))}
    </>
  );
};
