import React, { PropsWithChildren, FC, useState } from 'react';
import { RefreshControl, SafeAreaView, ScrollView, StyleProp, StyleSheet, View, ViewStyle } from 'react-native';
import Loader from '../Loader/Loader';
import { Header } from '../Header/Header';
import { IconName } from '../../assets/icons';
import { space } from '../../utils/colors';

type Props = {
  title?: string;
  leftIconName?: IconName;
  rightIconName?: IconName;
  onPressLeftIcon?: () => void;
  onPressRightIcon?: () => void;
  isLoading?: boolean;
  containerStyle?: StyleProp<ViewStyle>;
  onRefresh?: () => Promise<void>;
  isScrollView?: boolean;
};

const Layout: FC<PropsWithChildren<Props>> = ({
  title,
  leftIconName,
  rightIconName,
  onPressLeftIcon,
  onPressRightIcon,
  isLoading = false,
  containerStyle,
  onRefresh,
  isScrollView = false,
  children,
}) => {
  const [refreshing, setRefreshing] = useState(false);

  const onRefreshControl = async () => {
    setRefreshing(true);
    if (onRefresh) {
      await onRefresh();
    }
    setRefreshing(false);
  };

  let content = <View style={[styles.container, containerStyle]}>{children}</View>;

  if (isScrollView) {
    content = (
      <ScrollView
        style={[styles.container, containerStyle]}
        refreshControl={onRefresh && <RefreshControl refreshing={refreshing} onRefresh={onRefreshControl} />}
      >
        {children}
      </ScrollView>
    );
  }

  return (
    <SafeAreaView style={styles.safeArea}>
      {isLoading && <Loader />}
      {!!title && (
        <Header
          title={title}
          leftIconName={leftIconName}
          rightIconName={rightIconName}
          onPressLeftIcon={onPressLeftIcon}
          onPressRightIcon={onPressRightIcon}
        />
      )}
      {content}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  scrollViewContainer: {
    height: '100%',
    width: '100%',
    padding: space.small,
  },
  container: {
    flex: 1,
    width: '100%',
    padding: space.small,
  },
});

export default Layout;
