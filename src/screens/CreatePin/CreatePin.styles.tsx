import { StyleSheet } from 'react-native';
import { color, fontSize, space } from '../../utils/colors';

export const useCreatePinStyles = () => {
  return StyleSheet.create({
    layout: {
      paddingTop: 100,
    },
    title: {
      alignSelf: 'center',
      marginBottom: space.large,
      fontSize: fontSize.medium,
      fontWeight: '500',
    },
    description: {
      fontSize: fontSize.xMedium,
      color: color.grey5,
      textAlign: 'center',
      padding: space.xLarge,
    },
    error: {
      color: color.red2,
    },
    keyboardWrapper: {
      flex: 1,
      justifyContent: 'flex-end',
      paddingBottom: space.large,
    },
  });
};
