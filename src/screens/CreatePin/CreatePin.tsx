import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import Layout from '../../components/Layout/Layout';
import { Keyboard } from '../../components/Keyboard/Keyboard';
import { useCreatePinStyles } from './CreatePin.styles';
import { isEqual, isEmpty } from 'lodash';
import { PINBoxes } from '../../components/PINBoxes/PINBoxes';
import { useAppNavigation } from '../../hooks/navigation';
import { ScreenName, StackName } from '../../@types/navigation';
import { setObject } from '../../secureStorage/secureStorage';
import { PIN } from '../../secureStorage/secureStorage.types';
import { useAppDispatch } from '../../store/rootStore';
import { setIsOnboarded } from '../../store/user/user.slice';

const defaultPin = ['', '', '', ''];
const descriptionText = 'Passcode adds an extra layer of security when using the app';
const createPin = 'Create passcode';
const confirmPin = 'Confirm passcode';
const errorMessage = "Those passwords didn't match!";
const lastIndex = 3;

export const CreatePin = () => {
  const navigation = useAppNavigation();
  const styles = useCreatePinStyles();
  const [isConfirmation, setIsConfirmation] = useState(false);
  const [pin, setPin] = useState<string[]>(defaultPin);
  const [pinConfirmation, setPinConfirmation] = useState<string[]>(defaultPin);
  const [error, setError] = useState('');
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!isEmpty(pin[lastIndex])) {
      setIsConfirmation(true);
    }
  }, [pin]);

  useEffect(() => {
    const pinCorrect = async () => {
      await setObject(PIN, pin);
      dispatch(setIsOnboarded(true));
      navigation.replace(StackName.HOME_STACK, { screen: ScreenName.PORTFOLIO });
    };

    const pinIncorrect = () => {
      setIsConfirmation(false);
      setPin(defaultPin);
      setPinConfirmation(defaultPin);
      setError(errorMessage);
    };

    if (!isEmpty(pinConfirmation[lastIndex])) {
      if (isEqual(pin, pinConfirmation)) {
        pinCorrect();
      } else {
        pinIncorrect();
      }
    }
  }, [pin, pinConfirmation, navigation, dispatch]);

  const onPressKey = (key: string | number) => {
    if (typeof key !== 'number') {
      if (key === 'remove') {
        clearLastPin();
      }
    } else {
      addPin(key);
    }
  };

  const clearLastPin = () => {
    if (isConfirmation) {
      const pins = [...pinConfirmation];
      const firstEmptyIndex = pins.findIndex(firstKey => isEmpty(firstKey));
      pins[firstEmptyIndex - 1] = '';

      setPinConfirmation(pins);
    } else {
      const pins = [...pin];
      const firstEmptyIndex = pins.findIndex(firstKey => isEmpty(firstKey));
      pins[firstEmptyIndex - 1] = '';

      setPin(pins);
    }
  };

  const addPin = (key: number) => {
    if (isConfirmation) {
      const pins = [...pinConfirmation];
      const firstEmptyIndex = pins.findIndex(firstKey => isEmpty(firstKey));
      pins[firstEmptyIndex] = key.toString();

      setPinConfirmation(pins);
    } else {
      const pins = [...pin];
      const firstEmptyIndex = pins.findIndex(firstKey => isEmpty(firstKey));
      pins[firstEmptyIndex] = key.toString();

      setPin(pins);
    }
  };

  const title = isConfirmation ? confirmPin : createPin;
  const description = error ? error : descriptionText;

  return (
    <Layout containerStyle={styles.layout}>
      <Text style={styles.title}>{title}</Text>

      <PINBoxes pin={isConfirmation ? pinConfirmation : pin} />

      <Text style={[styles.description, !!error && styles.error]}>{description}</Text>

      <View style={styles.keyboardWrapper}>
        <Keyboard onPressKey={onPressKey} />
      </View>
    </Layout>
  );
};
