import { StyleSheet } from 'react-native';
import { fontSize, color, space } from '../../utils/colors';

export const useReceiveStyles = () => {
  return StyleSheet.create({
    addressContainer: {
      flex: 1,

      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginTop: space.medium,
    },
    address: {
      fontSize: fontSize.large,
      color: color.grey1,
      fontWeight: '500',
      textAlign: 'center',
      marginRight: space.large,
      flex: 9,
    },
    input: {
      marginTop: space.large,
    },
    QRCodeWrapper: {
      marginTop: space.xLarge,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
};
