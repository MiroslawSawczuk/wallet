import React, { useState } from 'react';
import { Text, TouchableOpacity, useWindowDimensions } from 'react-native';
import { useReceiveStyles } from './Receive.styles';
import Layout from '../../components/Layout/Layout';
import { BaseInput } from '../../components/BaseInput/BaseInput';
import { useAppNavigation } from '../../hooks/navigation';
import { useAppSelector } from '../../store/rootStore';
import { selectCurrentAsset } from '../../store/wallet/wallet.selectors';
import { Icon } from '../../components/Icon/Icon';
import QRCodeGenerate from '../../components/QRCodeGenerate/QRCodeGenerate';

export const Receive = () => {
  const navigation = useAppNavigation();
  const { width } = useWindowDimensions();
  const styles = useReceiveStyles();
  const [amount, setAmount] = useState('');

  const currentAsset = useAppSelector(selectCurrentAsset);
  const address = currentAsset.address;

  const onChangeAmount = (value: string) => {
    setAmount(value);
  };

  const onPressAddress = () => {
    ///TODO copy address to clipboard
  };

  const QRCodeData = { address: address, amount: amount };

  return (
    <Layout isScrollView title={'Receive '} leftIconName="arrowLeft" onPressLeftIcon={() => navigation.goBack()}>
      <TouchableOpacity style={styles.addressContainer} onPress={onPressAddress}>
        <Text style={styles.address}>{address}</Text>
        <Icon name="copy" size={30} />
      </TouchableOpacity>
      <BaseInput
        label={'Amount'}
        value={amount}
        onChangeText={onChangeAmount}
        keyboardType="numeric"
        wrapperStyles={styles.input}
      />

      <QRCodeGenerate data={QRCodeData} size={width * 0.7} containerStyle={styles.QRCodeWrapper} />
    </Layout>
  );
};
