import React from 'react';
import { Text, View } from 'react-native';
import { Icon } from '../../../../components/Icon/Icon';
import { useWalletNameSectionStyles } from './WalletNameSection.styles';
import { useAppSelector } from '../../../../store/rootStore';
import { selectWalletName } from '../../../../store/wallet/wallet.selectors';

export const WalletNameSection = () => {
  const styles = useWalletNameSectionStyles();
  const walletName = useAppSelector(selectWalletName);

  return (
    <View style={styles.walletOptionsContainer}>
      <Text style={styles.walletLabel}>{walletName}</Text>
      <Icon name="copy" />
    </View>
  );
};
