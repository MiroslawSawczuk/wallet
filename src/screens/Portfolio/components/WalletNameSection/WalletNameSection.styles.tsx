import { StyleSheet } from 'react-native';
import { color, fontSize } from '../../../../utils/colors';

export const useWalletNameSectionStyles = () => {
  return StyleSheet.create({
    walletOptionsContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    walletLabel: {
      fontSize: fontSize.medium,
      color: color.grey1,
      fontWeight: '500',
    },
  });
};
