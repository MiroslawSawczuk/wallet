import React, { FC } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { useAssetStyles } from './Asset.styles';

export type AssetType = {
  name: string;
  balance: number;
  symbol: string;
  value: number;
  currentPrice: number;
  price_change_24h: number;
  imageUrl: string;
  isPositive: boolean;
};
type Props = {
  asset: AssetType;
  onPress: () => void;
};
export const Asset: FC<Props> = ({ asset, onPress }) => {
  const styles = useAssetStyles();

  const dayChangeStyle = asset.isPositive ? styles.positive : styles.negative;

  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.leftContainer}>
        <View style={styles.circle}>
          <Image source={{ uri: asset?.imageUrl }} style={styles.image} />
        </View>
      </View>

      <View style={styles.content}>
        <Text style={styles.title}>
          {asset.name} ({asset.symbol})
        </Text>
        <View style={styles.marketPriceContainer}>
          <Text style={styles.marketPrice}>${asset?.currentPrice}</Text>
          <Text style={[styles.marketChange, dayChangeStyle]}>{asset?.price_change_24h.toFixed(2)}%</Text>
        </View>
      </View>

      <View style={styles.rightContainer}>
        <View style={styles.value}>
          <Text style={styles.text}>
            {asset.balance.toFixed(4)} {asset.symbol.toUpperCase()}
          </Text>
        </View>

        <View style={styles.change}>
          <Text>${asset.value.toFixed(2)}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};
