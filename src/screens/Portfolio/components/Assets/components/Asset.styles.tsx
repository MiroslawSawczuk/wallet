import { StyleSheet } from 'react-native';
import { color, radius, fontSize, space } from '../../../../../utils/colors';

export const useAssetStyles = () => {
  return StyleSheet.create({
    container: {
      height: 70,
      flexDirection: 'row',
      backgroundColor: color.grey9,
      borderRadius: radius.medium,
      paddingVertical: 10,
      paddingHorizontal: 20,
      marginBottom: space.medium,
    },

    leftContainer: {
      width: 50,
      justifyContent: 'center',
      alignItems: 'flex-start',
    },
    circle: {
      borderRadius: radius.xLarge,
      width: 30,
      height: 30,
      backgroundColor: color.blue9,
      justifyContent: 'center',
      alignItems: 'center',
    },
    image: {
      height: 25,
      width: 25,
    },
    content: {
      padding: 5,
      justifyContent: 'center',
      flex: 1,
    },
    title: {
      marginBottom: 5,
      fontSize: fontSize.medium,
      color: color.grey1,
      fontWeight: '500',
    },
    marketPrice: {
      fontSize: fontSize.small,
      color: color.grey1,
      fontWeight: '400',
    },
    marketChange: {
      fontSize: fontSize.small,
      color: color.grey1,
      fontWeight: '400',
      marginLeft: space.xxSmall,
    },
    marketPriceContainer: {
      flexDirection: 'row',
    },
    rightContainer: {
      width: 100,
    },

    value: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'flex-end',
    },
    change: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
    },
    text: {
      fontSize: fontSize.xMedium,
      color: color.grey1,
      fontWeight: '400',
    },
    positive: {
      color: color.green2,
    },
    negative: {
      color: color.red2,
    },
  });
};
