import React, { FC, useEffect, useState } from 'react';
import { ActivityIndicator, Text, View } from 'react-native';
import { useAssetsStyles } from './Assets.styles';
import { Asset } from '../../../../store/wallet/wallet.slice.types';
import { Asset as AssetComponent, AssetType } from './components/Asset';
import { ScreenName } from '../../../../@types/navigation';
import { useAppNavigation } from '../../../../hooks/navigation';
import { useAppDispatch, useAppSelector } from '../../../../store/rootStore';
import { setCurrentAsset } from '../../../../store/wallet/wallet.slice';
import { selectWallet } from '../../../../store/wallet/wallet.selectors';
import { MarketCoinDetails } from '../../../../store/coingecko/coingecko.api.types';
import { CoinType } from '../../../../@types/enums';
import { getBalance } from '../../../../services/web3.service';
import { getItem } from '../../../../secureStorage/secureStorage';
import { getTokenBalance } from '../../../../services/eth.service';
import { PRIVATE_KEY } from '../../../../secureStorage/secureStorage.types';

type Props = {
  coinDetails: MarketCoinDetails[] | undefined;
};
export const Assets: FC<Props> = ({ coinDetails }) => {
  const styles = useAssetsStyles();
  const navigation = useAppNavigation();
  const dispatch = useAppDispatch();
  const wallet = useAppSelector(selectWallet);
  const [displayAssets, setDisplayAssets] = useState<AssetType[]>();
  const [isLoading, setIsLoading] = useState(true);

  const onPressAsset = (asset: AssetType) => {
    // dispatch(setCurrentAsset(asset));
    // navigation.navigate(ScreenName.ASSET_OVERVIEW);
  };

  console.log('coinDetails', coinDetails?.length);

  useEffect(() => {
    initializeData();
  }, [coinDetails]);

  //TODO change this function to await PromiseAll
  const initializeData = async () => {
    const array: AssetType[] = [];
    setIsLoading(true);

    for (let index = 0; index < wallet.allAssets.length; index++) {
      const asset = wallet.allAssets[index];

      const coinAddress = wallet.userAssets.find(userAsset => userAsset.symbol === asset.symbol)?.address;

      if (!coinAddress) {
        continue;
      }
      let balance = 0;
      if (asset.coinType === CoinType.Coin) {
        const coinBalance = await getBalance(coinAddress);

        balance = Number(coinBalance);
      } else {
        const privateKey = (await getItem(PRIVATE_KEY)) || '';
        const tokenBalance = await getTokenBalance(coinAddress, asset.tokenAbi, privateKey, asset.tokenAddress || '');

        balance = Number(tokenBalance);
      }

      const marketAsset = coinDetails?.find(a => a.symbol === asset.symbol.toLowerCase());

      array.push({
        name: asset.name,
        balance,
        symbol: asset.symbol,
        value: marketAsset?.current_price ? marketAsset?.current_price * balance : 0,
        currentPrice: marketAsset?.current_price ?? 0,
        price_change_24h: marketAsset?.price_change_24h ?? 0,
        imageUrl: marketAsset?.image ?? '',
        isPositive: marketAsset?.price_change_percentage_24h ? marketAsset?.price_change_percentage_24h >= 0 : true,
      });
    }
    setDisplayAssets(array);
    setIsLoading(false);
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={styles.loaderWrapper}>
          <ActivityIndicator />
        </View>
      ) : (
        <>
          <Text style={styles.title}>Assets</Text>
          {displayAssets?.map(asset => (
            <AssetComponent key={asset.symbol} asset={asset} onPress={() => onPressAsset(asset)} />
          ))}
        </>
      )}
    </View>
  );
};
