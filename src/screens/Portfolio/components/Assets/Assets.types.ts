export type DisplayAsset = {
  ticker: string;
  address: string;
  name: string | undefined;
  logoUrl: string | undefined;
  value: number | undefined;
  dayChange: number | undefined;
};
