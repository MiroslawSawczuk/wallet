import { StyleSheet } from 'react-native';
import { color, fontSize, space } from '../../../../utils/colors';

export const useAssetsStyles = () => {
  return StyleSheet.create({
    container: {
      minHeight: 400,
    },
    title: {
      color: color.grey1,
      fontSize: fontSize.medium,
      fontWeight: '600',
      marginTop: space.large,
      marginBottom: space.xSmall,
    },
    image: {
      width: 25,
      height: 25,
    },
    loaderWrapper: {
      flex: 1,
      justifyContent: 'center',
    },
  });
};
