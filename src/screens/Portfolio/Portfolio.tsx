import React from 'react';
import Layout from '../../components/Layout/Layout';
import { Assets } from './components/Assets/Assets';
import { WalletNameSection } from './components/WalletNameSection/WalletNameSection';
import { useGetCoinDetailsQuery } from '../../store/coingecko/coingecko.api';
import { WalletBalance } from '../AssetOverview/components/WalletBalance/WalletBalance';

export const Portfolio = () => {
  const { data, refetch } = useGetCoinDetailsQuery(undefined);

  //TODO pobieranie wallet balance z API - bedzie bardziej stabilne?
  // Wysylanie transakcji za pomoca ether.js
  // tylko wysylanie za pomoca ether.js, reszta za pomoca api?
  // przejrzec caly kod
  // dodac kolejny token

  const onRefresh = async () => {
    await refetch();
  };

  return (
    <Layout isScrollView title="Portfolio" onRefresh={onRefresh}>
      <WalletNameSection />
      <WalletBalance coinDetails={data?.coinDetails} />
      <Assets coinDetails={data?.coinDetails} />
    </Layout>
  );
};
