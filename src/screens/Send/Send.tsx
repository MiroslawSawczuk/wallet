import React, { useState } from 'react';
import { sendCoin } from '../../services/web3.service';
import Layout from '../../components/Layout/Layout';
import { useAppNavigation } from '../../hooks/navigation';
import { getItem } from '../../secureStorage/secureStorage';
import { PRIVATE_KEY } from '../../secureStorage/secureStorage.types';
import { CodeScanner } from './components/CodeScanner';
import { isNumeric } from '../../utils/number.helper';
import { BaseInput } from '../../components/BaseInput/BaseInput';
import { useSendStyles } from './Send.styles';
import { useAppSelector } from '../../store/rootStore';
import { selectCurrentAsset, selectWallet } from '../../store/wallet/wallet.selectors';
import { BlockButton } from '../../components/BlockButton/BlockButton';
import { Icon } from '../../components/Icon/Icon';
import { color } from '../../utils/colors';
import { CoinType } from '../../@types/enums';
import { sendToken_ERC20 } from '../../services/eth.service';

const addressMobile = '0x4864b54F4184eAa33762b8c79367649c5c108fd0';
// const addressSimulator = '0xA60Eb7eA485210317F0e3B314822361429F44A82';
// const addressSimulatorPrivateKey = '0xab87bdd10f001d718a1b9cc35f2c3b111af2b84daf16389400e04161c4718e67';

export const Send = () => {
  const navigation = useAppNavigation();
  const styles = useSendStyles();
  const currentAsset = useAppSelector(selectCurrentAsset);
  const wallet = useAppSelector(selectWallet);
  const address = wallet.userAssets.find(a => a.symbol === currentAsset.symbol)?.address || '';

  const [amount, setAmount] = useState('');
  const [addressTo, setAddressTo] = useState(addressMobile);

  const [isLoading, setIsLoading] = useState(false);

  const onPressSend = async () => {
    setIsLoading(true);
    const privateKey = (await getItem(PRIVATE_KEY)) || '';

    if (!address || !privateKey) {
      return;
    }
    let isSend = false;
    if (currentAsset.coinType === CoinType.Coin) {
      isSend = await sendCoin(address, addressTo, Number(amount), privateKey);
      setIsLoading(false);
    } else {
      isSend = await sendToken_ERC20(
        privateKey,
        addressTo,
        currentAsset.tokenAddress || '',
        amount,
        currentAsset.tokenAbi,
      );
    }

    if (isSend) {
      navigation.goBack();
    }
  };

  const onCodeScanned = (scanAddress: string, scanAmount: string) => {
    setAmount(scanAmount);
    setAddressTo(scanAddress);
  };

  const onChangeAmount = (value: string) => {
    setAmount(value);
  };

  const onChangeAddress = (value: string) => {
    setAddressTo(value);
  };

  const btnSendEnabled = !!amount && isNumeric(amount) && !!addressTo;

  return (
    <Layout
      isScrollView
      title={`Send ${currentAsset.symbol}`}
      leftIconName="arrowLeft"
      onPressLeftIcon={() => navigation.goBack()}
      isLoading={isLoading}
    >
      <BaseInput
        label={'Amount'}
        value={amount}
        onChangeText={onChangeAmount}
        keyboardType="numeric"
        wrapperStyles={styles.input}
      />

      <BaseInput
        label={'Address'}
        value={addressTo}
        onChangeText={onChangeAddress}
        keyboardType="numeric"
        wrapperStyles={styles.input}
      />

      <BlockButton
        title="Send"
        leftIcon={<Icon name="send" color={color.white} size={24} />}
        onPress={onPressSend}
        containerStyle={styles.btnSend}
        disabled={!btnSendEnabled}
      />

      <CodeScanner onCodeScanned={onCodeScanned} />
    </Layout>
  );
};
