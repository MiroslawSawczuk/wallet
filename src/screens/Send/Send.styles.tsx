import { StyleSheet } from 'react-native';
import { space } from '../../utils/colors';

export const useSendStyles = () => {
  return StyleSheet.create({
    input: {
      marginTop: space.medium,
    },
    btnSend: {
      marginTop: space.medium,
    },
  });
};
