import React, { FC, useState } from 'react';
import { View } from 'react-native';
import { QRCodeScanner } from '../../../components/QRCodeScanner/QRCodeScanner';
import { BlockButton } from '../../../components/BlockButton/BlockButton';
import { Icon } from '../../../components/Icon/Icon';
import { color } from '../../../utils/colors';
import { useCodeScannerStyles } from './CodeScanner.styles';

type Props = {
  onCodeScanned: (scannedAddress: string, scannedAmount: string) => void;
};
export const CodeScanner: FC<Props> = ({ onCodeScanned }) => {
  const styles = useCodeScannerStyles();
  const [isCameraVisible, setIsCameraVisible] = useState(false);

  const onPressScanQRCode = () => {
    setIsCameraVisible(true);
  };

  const onCodeScannedSuccess = (scannedAddress: string, scannedAmount: string) => {
    onCodeScanned(scannedAddress, scannedAmount);
    setIsCameraVisible(false);
  };

  return (
    <View style={styles.container}>
      {!isCameraVisible ? (
        <BlockButton
          title="Scan QR code"
          leftIcon={<Icon name="camera" color={color.white} size={20} />}
          onPress={onPressScanQRCode}
          containerStyle={styles.btnCamera}
        />
      ) : (
        <QRCodeScanner onCodeScanned={onCodeScannedSuccess} />
      )}
    </View>
  );
};
