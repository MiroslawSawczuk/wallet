import { StyleSheet } from 'react-native';
import { space } from '../../../utils/colors';

export const useCodeScannerStyles = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    btnCamera: {
      marginTop: space.medium,
    },
  });
};
