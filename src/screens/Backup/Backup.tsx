import React from 'react';
import Layout from '../../components/Layout/Layout';
import { useAppNavigation } from '../../hooks/navigation';
import { useBackupStyles } from './Backup.styles';
import { BaseButton } from '../../components/BaseButton/BaseButton';
import { Text, View } from 'react-native';
import LottieView from 'lottie-react-native';
import BackupAnimation from '../../assets/lottie/backup.animation.json';
import { ScreenName } from '../../@types/navigation';

export const Backup = () => {
  const navigation = useAppNavigation();
  const styles = useBackupStyles();

  const onPressBackupManually = () => {
    navigation.navigate(ScreenName.SECRET_PHRASE);
  };

  return (
    <Layout title={'Backup'} leftIconName="arrowLeft" onPressLeftIcon={() => navigation.goBack()}>
      <View style={styles.topContainer}>
        <LottieView style={styles.walletAnimation} source={BackupAnimation} autoPlay />
        <Text style={styles.title}>Back up secret phrase</Text>
        <Text style={styles.description}>Protect your assets by backing up your seed phrase now.</Text>
      </View>

      <View style={styles.bottomContainer}>
        <BaseButton title={'Back up manually'} containerStyle={styles.btnBackup} onPress={onPressBackupManually} />
        <BaseButton title={'Back up to Google Drive'} disabled />
      </View>
    </Layout>
  );
};
