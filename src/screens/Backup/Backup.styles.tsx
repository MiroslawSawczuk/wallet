import { StyleSheet } from 'react-native';
import { color, fontSize, space } from '../../utils/colors';

export const useBackupStyles = () => {
  return StyleSheet.create({
    topContainer: {
      flex: 2,
      alignItems: 'center',
    },
    walletAnimation: {
      width: 200,
      height: 200,
    },
    title: {
      fontSize: fontSize.xxLarge,
      textAlign: 'center',
      fontWeight: '700',
      marginBottom: space.small,
    },
    description: {
      fontSize: fontSize.medium,
      textAlign: 'center',
      fontWeight: '400',
      color: color.grey5,
    },
    bottomContainer: {
      flex: 1,
      justifyContent: 'flex-end',
      paddingBottom: space.large,
    },
    btnBackup: {
      marginBottom: space.medium,
    },
  });
};
