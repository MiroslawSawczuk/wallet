import React, { FC } from 'react';
import { usePhraseItemStyles } from './PhraseItem.styles';
import { Text, View } from 'react-native';

type Props = {
  phrase: string;
};
export const PhraseItem: FC<Props> = ({ phrase }) => {
  const styles = usePhraseItemStyles();
  return (
    <View style={styles.container}>
      <Text style={styles.phrase}>{phrase}</Text>
    </View>
  );
};
