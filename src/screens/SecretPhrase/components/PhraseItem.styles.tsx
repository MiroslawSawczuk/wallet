import { StyleSheet, useWindowDimensions } from 'react-native';
import { space, fontSize, color, radius } from '../../../utils/colors';

export const usePhraseItemStyles = () => {
  const { width } = useWindowDimensions();
  return StyleSheet.create({
    container: {
      height: 30,
      width: (width - space.small * 3) / 2,
      backgroundColor: color.grey9,
      borderRadius: radius.xxSmall,
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: space.xSmall,
    },
    phrase: {
      fontSize: fontSize.medium,
      color: color.grey3,
      fontWeight: '500',
    },
  });
};
