import React, { useEffect, useState } from 'react';
import Layout from '../../components/Layout/Layout';
import { useAppNavigation } from '../../hooks/navigation';
import { useBackupStyles } from './SecretPhrase.styles';
import { BaseButton } from '../../components/BaseButton/BaseButton';
import { Text, TouchableOpacity, View } from 'react-native';
import { ScreenName } from '../../@types/navigation';
import { HDNodeWallet } from 'ethers';
import { createRandomWallet } from '../../services/eth.service';
import { PhraseItem } from './components/PhraseItem';
import { Icon } from '../../components/Icon/Icon';

export const SecretPhrase = () => {
  const navigation = useAppNavigation();
  const styles = useBackupStyles();

  const [wallet, setWallet] = useState<HDNodeWallet>();
  const [phrases, setPhrases] = useState<string[]>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    createWallet();
  }, []);

  const createWallet = async () => {
    try {
      setIsLoading(true);
      await new Promise(resolve => setTimeout(resolve, 1)); // Workaround - Fake trigger of isLoading

      const ethWallet = await createRandomWallet();
      const phraseArray = ethWallet?.mnemonic?.phrase.split(' ');

      setPhrases(phraseArray);
      setWallet(ethWallet);
    } catch (error) {
      console.error('Error creating wallet:', error);
    } finally {
      setIsLoading(false);
    }
  };

  const onPressContinue = () => {
    navigation.navigate(ScreenName.SECRET_PHRASE_REPEAT, { wallet });
  };

  return (
    <Layout
      title={'Secret phrase'}
      leftIconName="arrowLeft"
      isLoading={isLoading}
      onPressLeftIcon={() => navigation.goBack()}
    >
      <View style={styles.topContainer}>
        {!!phrases && (
          <>
            <View style={styles.phrasesContainer}>
              <View>
                {phrases?.slice(0, 6).map((phrase, index) => (
                  <PhraseItem key={phrase} phrase={`${index + 1}.${phrase}`} />
                ))}
              </View>
              <View>
                {phrases?.slice(6, 12).map((phrase, index) => (
                  <PhraseItem key={phrase} phrase={`${index + 7}. ${phrase}`} />
                ))}
              </View>
            </View>

            <TouchableOpacity style={styles.copyRow}>
              <Icon name="copy" />
              <Text>Copy to clipboard</Text>
            </TouchableOpacity>
          </>
        )}
      </View>
      <BaseButton title={'Continue'} onPress={onPressContinue} containerStyle={styles.btnContinue} />
    </Layout>
  );
};
