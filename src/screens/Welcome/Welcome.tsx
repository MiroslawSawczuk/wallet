import React, { useCallback, useEffect } from 'react';
import Layout from '../../components/Layout/Layout';
import { ScreenName } from '../../@types/navigation';
import { useAppNavigation } from '../../hooks/navigation';
import { DevSettings, Text, View } from 'react-native';
import LottieView from 'lottie-react-native';
import { useWelcomeStyles } from './Welcome.styles';
import { BlockButton } from '../../components/BlockButton/BlockButton';
import Wallet from '../../assets/lottie/wallet.animation.json';
import { color } from '../../utils/colors';
import { Icon } from '../../components/Icon/Icon';
import { useFocusEffect } from '@react-navigation/native';
import { useAppDispatch } from '../../store/rootStore';
import { coingeckoBaseApi } from '../../store/coingeckoBaseApi';
import { initializeEthService } from '../../services/eth.service';
import { etherscanSepoliaBaseApi } from '../../store/etherscanSepoliaBaseApi';
import { baseApi } from '../../store/baseApi';

export const Welcome = () => {
  const navigation = useAppNavigation();
  const styles = useWelcomeStyles();
  const dispatch = useAppDispatch();

  useEffect(() => {
    initializeEthService();
    setCustomDevSettings();
  }, []);

  const setCustomDevSettings = () => {
    DevSettings.addMenuItem('Clear cache', () => {
      dispatch(baseApi.util.resetApiState());
      dispatch(coingeckoBaseApi.util.resetApiState());
      dispatch(etherscanSepoliaBaseApi.util.resetApiState());
    });
  };

  const onPressNewWallet = () => {
    navigation.navigate(ScreenName.BACKUP);
  };

  const onPressAddExistingWallet = () => {
    navigation.navigate(ScreenName.ADD_EXISTING_WALLET);
  };

  useFocusEffect(
    useCallback(() => {
      dispatch(coingeckoBaseApi.util.resetApiState());
    }, [dispatch]),
  );

  return (
    <Layout isScrollView={true}>
      <View style={styles.top}>
        <LottieView style={styles.walletAnimation} source={Wallet} autoPlay />
        <Text style={styles.welcomeText}>Join 70M+ people shaping the future of the internet with us</Text>
      </View>

      <BlockButton
        title="Create a new wallet"
        description="Generate a new multi-chain wallet"
        leftIcon={<Icon name="plus" color={color.white} />}
        onPress={onPressNewWallet}
        containerStyle={styles.newWalletButton}
      />

      <BlockButton
        title="Add existing wallet"
        description="Import, restore or view-only"
        leftIcon={<Icon name="arrowDown" color={color.white} size={24} />}
        onPress={onPressAddExistingWallet}
      />
    </Layout>
  );
};
