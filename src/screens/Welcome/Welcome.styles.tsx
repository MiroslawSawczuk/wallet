import { StyleSheet } from 'react-native';

export const useWelcomeStyles = () => {
  return StyleSheet.create({
    top: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    walletAnimation: {
      width: 200,
      height: 200,
    },
    welcomeText: {
      fontSize: 20,
      fontWeight: '500',
      textAlign: 'center',
      padding: 10,
    },
    newWalletButton: {
      marginTop: 30,
      marginBottom: 15,
    },
  });
};
