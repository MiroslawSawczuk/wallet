import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useSegmentControlStyles } from './SegmentControl.styles.';

export const SegmentControl = () => {
  const styles = useSegmentControlStyles();
  const selected = true;

  return (
    <View style={styles.container}>
      <TouchableOpacity style={[styles.chip, selected && styles.selectedChip]}>
        <Text style={selected && styles.selectedLabel}>Phrase</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.chip}>
        <Text>Keystore</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.chip}>
        <Text>Private key</Text>
      </TouchableOpacity>
    </View>
  );
};
