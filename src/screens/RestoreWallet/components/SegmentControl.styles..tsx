import { StyleSheet } from 'react-native';
import { color, space, radius } from '../../../utils/colors';

export const useSegmentControlStyles = () => {
  return StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      height: 40,
      borderWidth: 1,
      borderColor: color.grey8,
      borderRadius: radius.large,
      padding: space.xxSmall,
      marginBottom: space.large,
    },
    chip: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 30,
      borderColor: color.grey8,
      borderRadius: radius.large,
    },
    selectedChip: {
      backgroundColor: color.black,
    },
    selectedLabel: {
      color: color.white,
    },
  });
};
