import { StyleSheet } from 'react-native';
import { color, fontSize, space } from '../../utils/colors';

export const useRestoreWalletStyles = () => {
  return StyleSheet.create({
    input: {
      marginBottom: space.large,
    },
    textArea: {
      minHeight: 100,
      marginBottom: space.large,
    },
    bottomInfo: {
      fontSize: fontSize.xMedium,
      color: color.grey5,
      textAlign: 'center',
      paddingHorizontal: space.small,
    },
    footer: {
      flex: 1,
      justifyContent: 'flex-end',
      paddingBottom: space.large,
    },
  });
};
