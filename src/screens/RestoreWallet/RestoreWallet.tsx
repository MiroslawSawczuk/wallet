import React, { useState } from 'react';
import Layout from '../../components/Layout/Layout';
import { RestoreWalletRouteProps, ScreenName } from '../../@types/navigation';
import { useAppNavigation } from '../../hooks/navigation';
import { useRoute } from '@react-navigation/native';
import { toUpperCaseFirstLetter } from '../../utils/string.helper';
import { BaseInput } from '../../components/BaseInput/BaseInput';
import { useRestoreWalletStyles } from './RestoreWallet.styles';
import { SegmentControl } from './components/SegmentControl';
import { Alert, Text, View } from 'react-native';
import { BaseButton } from '../../components/BaseButton/BaseButton';
import { PRIVATE_KEY } from '../../secureStorage/secureStorage.types';
import { createWalletFromSeeds } from '../../services/eth.service';
import { setItem } from '../../secureStorage/secureStorage';
import Loader from '../../components/Loader/Loader';
import { useAppDispatch, useAppSelector } from '../../store/rootStore';
import { addWalletAsset, setWalletName as storeWalletName } from '../../store/wallet/wallet.slice';
import { TokenStandard } from '../../@types/enums';
import { selectWalletName } from '../../store/wallet/wallet.selectors';

const MOCK_real_hardcoded_seeds = 'obscure harsh announce hungry alien jungle require virus bulb plug hour elite';

export const RestoreWallet = () => {
  const navigation = useAppNavigation();
  const styles = useRestoreWalletStyles();
  const { params } = useRoute<RestoreWalletRouteProps>();
  const dispatch = useAppDispatch();
  const walletName = useAppSelector(selectWalletName);

  const [phrases, setPhrases] = useState(MOCK_real_hardcoded_seeds);
  const [isLoading, setIsLoading] = useState(false);

  const onPressLeftIcon = () => {
    navigation.goBack();
  };

  const onChangeWalletName = (name: string) => {
    dispatch(storeWalletName(name));
  };

  const onChangePhrases = (secretPhrases: string) => {
    setPhrases(secretPhrases);
  };

  const onPressRestoreWallet = async () => {
    setIsLoading(true);
    await new Promise(resolve => setTimeout(resolve, 1)); // Workaround - Fake trigger of isLoading

    const wallet = await createWalletFromSeeds(phrases);

    if (wallet && wallet.address && wallet.privateKey) {
      dispatch(addWalletAsset({ address: wallet.address, symbol: 'ETH', tokenStandard: TokenStandard.ERC20 }));
      dispatch(addWalletAsset({ address: wallet.address, symbol: 'LINK', tokenStandard: TokenStandard.ERC20 }));

      await setItem(PRIVATE_KEY, wallet.privateKey);

      navigation.navigate(ScreenName.CREATE_PIN);
    } else {
      Alert.alert('Provided phrases not recognized');
    }

    setIsLoading(false);
  };

  return (
    <Layout
      title={`Restore ${toUpperCaseFirstLetter(params.networkName)}`}
      leftIconName="arrowLeft"
      onPressLeftIcon={onPressLeftIcon}
      isLoading={isLoading}
    >
      {isLoading && <Loader />}
      <BaseInput
        label={'Wallet name'}
        value={walletName}
        onChangeText={onChangeWalletName}
        containerStyles={styles.input}
      />
      <SegmentControl />

      <BaseInput
        label={'Secret phrase'}
        value={phrases}
        onChangeText={onChangePhrases}
        containerStyles={styles.textArea}
        multiline
      />
      <Text style={styles.bottomInfo}>Typically 12 (sometimes 18, 24) words separated by single spaces</Text>

      <View style={styles.footer}>
        <BaseButton title="Restore wallet" onPress={onPressRestoreWallet} />
      </View>
    </Layout>
  );
};
