import { StyleSheet } from 'react-native';
import { color, fontSize, space } from '../../utils/colors';

export const useSecretPhraseConfirmStyles = () => {
  return StyleSheet.create({
    topContainer: {
      flex: 2,
    },
    title: {
      fontSize: fontSize.medium,
      fontWeight: '400',
      color: color.grey5,
      marginBottom: space.xLarge,
    },
    walletNameInput: {
      marginBottom: space.large,
      fontSize: fontSize.xLarge,
      fontWeight: '500',
      color: color.grey4,
    },
    textArea: {
      minHeight: 100,
      marginBottom: space.large,
      fontSize: fontSize.xLarge,
      fontWeight: '500',
      color: color.grey4,
    },
    btnConfirm: {
      marginBottom: space.large,
    },
  });
};
