import React, { useState } from 'react';
import Layout from '../../components/Layout/Layout';
import { SecretPhraseConfirmRouteProps, ScreenName } from '../../@types/navigation';
import { useAppNavigation } from '../../hooks/navigation';
import { BaseButton } from '../../components/BaseButton/BaseButton';
import { Alert, Text, View } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { PRIVATE_KEY } from '../../secureStorage/secureStorage.types';
import { setItem } from '../../secureStorage/secureStorage';
import { BaseInput } from '../../components/BaseInput/BaseInput';
import { useSecretPhraseConfirmStyles } from './SecretPhraseConfirm.styles';
import { useAppDispatch, useAppSelector } from '../../store/rootStore';
import { addWalletAsset, setWalletName as storeWalletName } from '../../store/wallet/wallet.slice';
import { TokenStandard } from '../../@types/enums';
import { selectWalletName } from '../../store/wallet/wallet.selectors';

export const SecretPhraseConfirm = () => {
  const navigation = useAppNavigation();
  const dispatch = useAppDispatch();
  const styles = useSecretPhraseConfirmStyles();
  const { params } = useRoute<SecretPhraseConfirmRouteProps>();
  const walletName = useAppSelector(selectWalletName);
  const wallet = params.wallet;

  const [phrases, setPhrases] = useState<string>(wallet.mnemonic?.phrase ?? '');

  const validatePhrases = () => {
    return phrases === wallet.mnemonic?.phrase;
  };

  const onPressNext = async () => {
    const isPhrasesValid = validatePhrases();

    if (isPhrasesValid && wallet) {
      dispatch(addWalletAsset({ address: wallet.address, symbol: 'ETH', tokenStandard: TokenStandard.ERC20 }));
      dispatch(addWalletAsset({ address: wallet.address, symbol: 'LINK', tokenStandard: TokenStandard.ERC20 }));

      await setItem(PRIVATE_KEY, wallet.privateKey);

      navigation.navigate(ScreenName.CREATE_PIN);
    } else {
      Alert.alert("Provided phrases don't match");
    }
  };

  const onChangeWalletName = (name: string) => {
    dispatch(storeWalletName(name));
  };

  const onChangeSeedPhrase = (walletPhrases: string) => {
    setPhrases(walletPhrases);
  };

  return (
    <Layout title={'Confirm secret phrase'} leftIconName="arrowLeft" onPressLeftIcon={() => navigation.goBack()}>
      <View style={styles.topContainer}>
        <BaseInput
          label={'Wallet name'}
          value={walletName}
          onChangeText={onChangeWalletName}
          containerStyles={styles.walletNameInput}
        />
        <Text style={styles.title}>Type your seed phrases separate them with space</Text>

        <BaseInput
          label={'Phrases'}
          value={phrases}
          onChangeText={onChangeSeedPhrase}
          containerStyles={styles.textArea}
          multiline
        />
      </View>

      <BaseButton title={'Next'} onPress={onPressNext} containerStyle={styles.btnConfirm} />
    </Layout>
  );
};
