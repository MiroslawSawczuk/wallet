import { StyleSheet } from 'react-native';

export const useAddExistingWalletStyles = () => {
  return StyleSheet.create({
    button: {
      marginBottom: 15,
    },
  });
};
