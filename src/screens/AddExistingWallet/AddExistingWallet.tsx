import React from 'react';
import Layout from '../../components/Layout/Layout';
import { ScreenName } from '../../@types/navigation';
import { useAppNavigation } from '../../hooks/navigation';
import { BlockButton } from '../../components/BlockButton/BlockButton';
import { useAddExistingWalletStyles } from './AddExistingWallet.styles';
import { color } from '../../utils/colors';
import { Icon } from '../../components/Icon/Icon';

export const AddExistingWallet = () => {
  const navigation = useAppNavigation();
  const styles = useAddExistingWalletStyles();

  const onPressLeftIcon = () => {
    navigation.goBack();
  };

  const onPressSecretPhrase = () => {
    navigation.navigate(ScreenName.SELECT_NETWORK);
  };

  return (
    <Layout
      isScrollView={true}
      title={'Add existing wallet'}
      leftIconName="arrowLeft"
      onPressLeftIcon={onPressLeftIcon}
    >
      <BlockButton
        title="Secret phrase"
        description="Use a 12, 18, 24-word seed phrase"
        leftIcon={<Icon name="plus" color={color.white} size={24} />}
        rightIcon={<Icon name="chevronRight" color={color.blue5} size={30} />}
        onPress={onPressSecretPhrase}
        containerStyle={styles.button}
      />
      <BlockButton
        title="Google drive backup"
        description="Restore from Google Drive backup"
        leftIcon={<Icon name="cloudDownload" color={color.white} size={18} />}
        rightIcon={<Icon name="chevronRight" color={color.blue5} size={30} />}
        onPress={() => {}}
        containerStyle={styles.button}
        disabled
      />
      <BlockButton
        title="View-only wallet"
        description="Observe or track assets of another wallets"
        leftIcon={<Icon name="eye" color={color.white} size={18} />}
        rightIcon={<Icon name="chevronRight" color={color.blue5} size={30} />}
        onPress={() => {}}
        containerStyle={styles.button}
        disabled
      />
    </Layout>
  );
};
