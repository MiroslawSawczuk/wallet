import React, { useCallback, useState } from 'react';
import Layout from '../../components/Layout/Layout';
import { useFocusEffect } from '@react-navigation/native';
import { ActionButtons } from './components/ActionButtons/ActionButtons';
import { getBalance } from '../../services/web3.service';
import { useAppNavigation } from '../../hooks/navigation';
import { TransactionHistory } from '../../components/TransactionHistory/TransactionHistory';
import { useAppSelector } from '../../store/rootStore';
import { selectCurrentAsset, selectWallet } from '../../store/wallet/wallet.selectors';
import { CoinType } from '../../@types/enums';
import { getTokenBalance } from '../../services/eth.service';
import { getItem } from '../../secureStorage/secureStorage';
import { PRIVATE_KEY } from '../../secureStorage/secureStorage.types';
import { WalletBalance } from '../Portfolio/components/WalletBalance';

export const AssetOverview = () => {
  const navigation = useAppNavigation();
  const wallet = useAppSelector(selectWallet);
  const currentAsset = useAppSelector(selectCurrentAsset);

  const asset = wallet.userAssets.find(a => a.symbol === currentAsset.symbol);
  const coinAddress = asset?.address;

  const [isLoading, setIsLoading] = useState(false);
  const [balance, setBalance] = useState(0);

  useFocusEffect(
    useCallback(() => {
      setCoinBalance();
    }, [asset]),
  );

  const setCoinBalance = async () => {
    if (!coinAddress) {
      return;
    }
    setIsLoading(true);

    if (currentAsset.coinType === CoinType.Coin) {
      const coinBalance = await getBalance(coinAddress);
      setBalance(Number(coinBalance));
    } else {
      const privateKey = (await getItem(PRIVATE_KEY)) || '';
      const tokenBalance = await getTokenBalance(
        coinAddress,
        currentAsset.tokenAbi,
        privateKey,
        currentAsset.tokenAddress || '',
      );
      setBalance(Number(tokenBalance));
    }

    setIsLoading(false);
  };

  const onRefresh = async () => {
    await setCoinBalance();
  };

  return (
    <Layout
      isScrollView
      title={`${currentAsset.symbol} wallet`}
      leftIconName="arrowLeft"
      onPressLeftIcon={() => navigation.goBack()}
      isLoading={isLoading}
      onRefresh={onRefresh}
    >
      {/* <WalletBalance balance={Number(balance).toFixed(4)} currencyCode={currentAsset.symbol} /> */}

      <ActionButtons />

      {coinAddress ? (
        <TransactionHistory
          address={coinAddress}
          ticker={currentAsset.symbol}
          balance={balance}
          contractAddress={currentAsset.tokenAddress}
        />
      ) : null}
    </Layout>
  );
};
