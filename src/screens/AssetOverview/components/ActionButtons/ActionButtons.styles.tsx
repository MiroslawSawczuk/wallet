import { StyleSheet } from 'react-native';
import { space } from '../../../../utils/colors';

export const useActionButtonsStyles = () => {
  return StyleSheet.create({
    container: {
      marginTop: 50,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: space.xLarge,
    },
  });
};
