import React from 'react';
import { View } from 'react-native';
import { ScreenName } from '../../../../@types/navigation';
import { useAppNavigation } from '../../../../hooks/navigation';
import { ActionButton } from '../../../../components/ActionButton/ActionButton';
import { useActionButtonsStyles } from './ActionButtons.styles';

export const ActionButtons = () => {
  const navigation = useAppNavigation();
  const styles = useActionButtonsStyles();

  const onPressReceive = () => {
    navigation.navigate(ScreenName.RECEIVE);
  };

  const onPressSend = () => {
    navigation.navigate(ScreenName.SEND);
  };

  const onPressSwap = () => {
    //TODO TBI
  };

  return (
    <View style={styles.container}>
      <ActionButton title={'Receive'} iconName={'receive'} onPress={onPressReceive} />
      <ActionButton title={'Send'} iconName={'send'} onPress={onPressSend} />
      <ActionButton title={'Swap'} iconName={'swap'} onPress={() => onPressSwap} disabled />
    </View>
  );
};
