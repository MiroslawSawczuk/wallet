import React, { FC, useEffect, useState } from 'react';
import { ActivityIndicator, Text, View } from 'react-native';
import { CoinType } from '../../../../@types/enums';
import { getItem } from '../../../../secureStorage/secureStorage';
import { PRIVATE_KEY } from '../../../../secureStorage/secureStorage.types';
import { getTokenBalance } from '../../../../services/eth.service';
import { MarketCoinDetails } from '../../../../store/coingecko/coingecko.api.types';
import { useAppSelector } from '../../../../store/rootStore';
import { selectWallet } from '../../../../store/wallet/wallet.selectors';
import { getBalance } from '../../../../services/web3.service'; //TODO change to eth
import { useWalletBalanceStyles } from './WalletBalance.styles';

type Props = {
  coinDetails: MarketCoinDetails[] | undefined;
};

export const WalletBalance: FC<Props> = ({ coinDetails }) => {
  const styles = useWalletBalanceStyles();
  const [balance, setBalance] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const wallet = useAppSelector(selectWallet);

  useEffect(() => {
    setPortfolioBalance();
  }, [coinDetails]);

  const setPortfolioBalance = async () => {
    if (!coinDetails) {
      return;
    }
    let portfolioBalance = 0;
    await Promise.all(
      wallet.allAssets.map(async asset => {
        const coinAddress = wallet.userAssets.find(userAsset => userAsset.symbol === asset.symbol)?.address || '';

        let coinBalance = '0';
        if (asset.coinType === CoinType.Coin) {
          coinBalance = (await getBalance(coinAddress)) || '0';
        } else {
          const privateKey = (await getItem(PRIVATE_KEY)) || '0';
          coinBalance =
            (await getTokenBalance(coinAddress, asset.tokenAbi, privateKey, asset.tokenAddress || '')) || '';
        }

        const marketCoinData = coinDetails.find(coin => coin.symbol === asset.symbol.toLocaleLowerCase());
        if (coinBalance && marketCoinData) {
          portfolioBalance += Number(coinBalance) * marketCoinData?.current_price;

          setBalance(portfolioBalance);
        }
      }),
    );
    setIsLoading(false);
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <Text style={styles.balanceText}>
          {balance.toFixed(2)} {'USD'}
        </Text>
      )}
    </View>
  );
};
