import { StyleSheet } from 'react-native';

export const useWalletBalanceStyles = () => {
  return StyleSheet.create({
    container: {
      alignItems: 'center',
      marginTop: 50,
      height: 50,
    },
    balanceText: {
      fontSize: 30,
      fontWeight: '700',
    },
  });
};
