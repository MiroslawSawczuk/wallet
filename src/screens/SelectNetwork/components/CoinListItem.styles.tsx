import { StyleSheet } from 'react-native';
import { fontSize, space } from '../../../utils/colors';

export const useCoinListItemStyles = (disabled: boolean) => {
  return StyleSheet.create({
    container: {
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      paddingVertical: space.small,
      paddingHorizontal: space.medium,
      opacity: disabled ? 0.6 : 1,
    },
    imageContainer: {
      flex: 1,
    },
    image: {
      width: 30,
      height: 30,
    },
    name: {
      flex: 5,
      paddingLeft: space.medium,
      fontSize: fontSize.xLarge,
      opacity: disabled ? 0.6 : 1,
    },
  });
};
