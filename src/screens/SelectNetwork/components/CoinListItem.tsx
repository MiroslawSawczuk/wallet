import React, { FC } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { useCoinListItemStyles } from './CoinListItem.styles';
import { MarketCoinDetails } from '../../../store/coingecko/coingecko.api.types';

type Props = {
  coin: MarketCoinDetails;
  disabled?: boolean;
  onPress: (id: string) => void;
};
export const CoinListItem: FC<Props> = ({ coin, disabled = false, onPress }) => {
  const styles = useCoinListItemStyles(disabled);

  return (
    <TouchableOpacity onPress={() => onPress(coin.id)} style={styles.container} disabled={disabled}>
      <View style={styles.imageContainer}>
        <Image source={{ uri: coin?.image }} style={styles.image} />
      </View>

      <Text style={styles.name}>{coin?.name}</Text>
    </TouchableOpacity>
  );
};
