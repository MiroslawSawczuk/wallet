import React, { useCallback } from 'react';
import Layout from '../../components/Layout/Layout';
import { useAppNavigation } from '../../hooks/navigation';
import { ScreenName } from '../../@types/navigation';
import { CoinListItem } from './components/CoinListItem';
import { useGetCoinDetailsQuery } from '../../store/coingecko/coingecko.api';
import { useFocusEffect } from '@react-navigation/native';
import { BaseButton } from '../../components/BaseButton/BaseButton';

export const SelectNetwork = () => {
  const navigation = useAppNavigation();
  const { data, isFetching, refetch } = useGetCoinDetailsQuery(undefined);

  useFocusEffect(
    useCallback(() => {
      refetch();
    }, [refetch]),
  );

  const onPressLeftIcon = () => {
    navigation.goBack();
  };

  const onPressNetwork = (id: string) => {
    navigation.navigate(ScreenName.RESTORE_WALLET, { networkName: id });
  };

  const onPressETH = () => {
    navigation.navigate(ScreenName.RESTORE_WALLET, { networkName: 'Ethereum' });
  };

  return (
    <Layout
      isScrollView={true}
      isLoading={isFetching}
      title={'Select Network'}
      leftIconName="arrowLeft"
      onPressLeftIcon={onPressLeftIcon}
    >
      {data?.coinDetails?.map(coin => (
        <CoinListItem key={coin.name} coin={coin} onPress={onPressNetwork} disabled={coin.symbol !== 'eth'} />
      ))}
      <BaseButton title="ETH" onPress={onPressETH} />
    </Layout>
  );
};
