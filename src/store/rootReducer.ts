import { combineReducers } from '@reduxjs/toolkit';
import { baseApi } from './baseApi';
import walletSlice from './wallet/wallet.slice';
import { coingeckoBaseApi } from './coingeckoBaseApi';
import { etherscanSepoliaBaseApi } from './etherscanSepoliaBaseApi';
import userSlice from './user/user.slice';

export const rootReducer = combineReducers({
  [baseApi.reducerPath]: baseApi.reducer,
  [coingeckoBaseApi.reducerPath]: coingeckoBaseApi.reducer,
  [etherscanSepoliaBaseApi.reducerPath]: etherscanSepoliaBaseApi.reducer,
  wallet: walletSlice,
  user: userSlice,
});
