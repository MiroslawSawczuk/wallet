import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const coingeckoBaseQuery = fetchBaseQuery({
  baseUrl: 'https://api.coingecko.com/api/v3/',
});

export const coingeckoBaseApi = createApi({
  reducerPath: 'coingeckoBaseApi',
  baseQuery: coingeckoBaseQuery,
  endpoints: () => ({}),
});
