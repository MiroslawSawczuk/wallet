import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const baseQuery = fetchBaseQuery({
  baseUrl: 'www.someBaseApi.com',
  prepareHeaders: headers => {
    const accessToken = 'MY_ACCESS_TOKEN';
    headers.set('Authorization', `Barer ${accessToken}`);
    headers.set('Content-Type', 'application/json; charset=UTF-8');

    return headers;
  },
});

export const baseApi = createApi({
  reducerPath: 'baseApi',
  baseQuery: baseQuery,
  endpoints: () => ({}),
});
