import { etherscanSepoliaBaseApi } from '../etherscanSepoliaBaseApi';
import {
  GetTransactionsHistoryByAddressResponse,
  GetTransactionsHistoryByAddressRequest,
} from './etherscanSepolia.api.types';

const ETHERSCAN_API_KEY = 'Z7DE5KHE21X9F4VJDQVP5HA11H6V4145HA';

export const etherscanSepoliaApi = etherscanSepoliaBaseApi.injectEndpoints({
  endpoints: builder => ({
    getTransactionsHistoryByAddress: builder.query<
      GetTransactionsHistoryByAddressResponse,
      GetTransactionsHistoryByAddressRequest
    >({
      query: ({ address, contractAddress }) => ({
        method: 'GET',
        url: contractAddress
          ? `?module=account&action=tokentx&contractaddress=${contractAddress}&address=${address}&page=1 &offset=100&startblock=0&endblock=27025780&sort=asc&apikey=${ETHERSCAN_API_KEY}`
          : `?module=account&action=txlist&address=${address}&startblock=0&endblock=99999999&page=1&offset=10&sort=desc&apikey=${ETHERSCAN_API_KEY}`,
      }),
    }),
  }),
});

export const { useGetTransactionsHistoryByAddressQuery } = etherscanSepoliaApi;
