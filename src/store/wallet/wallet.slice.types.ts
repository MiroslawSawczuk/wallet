import { CoinType, TokenStandard } from '../../@types/enums';

export type UserAsset = {
  address: string;
  tokenStandard: TokenStandard;
  symbol: string;
};

export type Asset = {
  name: string;
  symbol: string;
  coinType: CoinType;

  tokenAddress?: string;
  tokenAbi?: any; //TODO adjust correct type JSON?
  tokenStandard?: TokenStandard;
};
