import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../rootStore';

const selectWalletStore = (state: RootState) => state.wallet;

export const selectWallet = createSelector(selectWalletStore, state => state);
export const selectAddress = createSelector(selectWalletStore, state => state.userAssets);

// export const selectWalletByAddress = createSelector(
//   selectWalletStore,
//   (_: unknown, address: string) => address,
//   (wallet, address) => wallet.userAssets.find(a => a.address === address),
// );

export const selectWalletName = createSelector(selectWalletStore, state => state.name);
export const selectCurrentAsset = createSelector(selectWalletStore, state => state.currentAsset);
