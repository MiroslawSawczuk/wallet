import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import { UserAsset, Asset } from './wallet.slice.types';
import { CoinType, TokenStandard } from '../../@types/enums';
import LINKContractAbi from '../../assets/contracts/LINK.contractAbi.json';

export type WalletSlice = {
  name: string;
  userAssets: UserAsset[];
  allAssets: Asset[];
  currentAsset: Asset;
};

const initialState: WalletSlice = {
  name: 'Wallet 1',
  userAssets: [] as UserAsset[],
  allAssets: [
    {
      name: 'Ethereum',
      symbol: 'ETH',
      coinType: CoinType.Coin,
    },
    {
      name: 'ChainLink',
      symbol: 'LINK',
      coinType: CoinType.Token,
      tokenAddress: '0x779877A7B0D9E8603169DdbD7836e478b4624789', //TODO get this data from .env
      tokenAbi: LINKContractAbi,
      tokenStandard: TokenStandard.ERC20,
    },
  ],
  currentAsset: {} as Asset,
};

export const walletSlice = createSlice({
  name: 'wallet',
  initialState: initialState,
  reducers: {
    setWalletName: (state, action: PayloadAction<string>) => {
      const name = action.payload;
      state.name = name;
    },
    addWalletAsset: (state, action: PayloadAction<UserAsset>) => {
      const address = action.payload;
      state.userAssets.push(address);
    },
    setCurrentAsset: (state, action: PayloadAction<Asset>) => {
      const asset = action.payload;
      state.currentAsset = asset;
    },
  },
});

export default walletSlice.reducer;

export const { setWalletName, addWalletAsset, setCurrentAsset } = walletSlice.actions;
