import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const etherscanSepoliaBaseQuery = fetchBaseQuery({
  baseUrl: 'https://api-sepolia.etherscan.io/api',
});

export const etherscanSepoliaBaseApi = createApi({
  reducerPath: 'etherscanSepoliaBaseApi',
  baseQuery: etherscanSepoliaBaseQuery,
  endpoints: () => ({}),
});
