import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../rootStore';

const selectUserSlice = (state: RootState) => state.user;

export const selectUser = createSelector(selectUserSlice, state => state);
