import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

export type UserSlice = {
  isOnboarded: boolean;
};

const initialState: UserSlice = {
  isOnboarded: false,
};

export const userSlice = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
    setIsOnboarded: (state, action: PayloadAction<boolean>) => {
      const isOnboarded = action.payload;
      state.isOnboarded = isOnboarded;
    },
  },
});

export default userSlice.reducer;

export const { setIsOnboarded } = userSlice.actions;
