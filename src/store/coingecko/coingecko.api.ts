import { coingeckoBaseApi } from '../coingeckoBaseApi';
import { GetCoinDetailsRequest, GetCoinDetailsResponse } from './coingecko.api.types';
const COINGECKO_API_KEY = 'CG-iyEdWHa5CNGdwKHTEHRVESxL';
const SUFFIX_CEDENTIALS = `&x_cg_demo_api_key=${COINGECKO_API_KEY}`;

export const coingeckoApi = coingeckoBaseApi.injectEndpoints({
  endpoints: builder => ({
    getCoinDetails: builder.query<GetCoinDetailsResponse, GetCoinDetailsRequest>({
      query: payload => ({
        method: 'GET',
        url: payload?.assetName
          ? `coins/markets?vs_currency=usd&ids=${payload.assetName}&order=market_cap_desc&per_page=100&page=1&sparkline=false&locale=en${SUFFIX_CEDENTIALS}`
          : `coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false&locale=en${SUFFIX_CEDENTIALS}`,
      }),
      transformResponse: (response: GetCoinDetailsResponse) => {
        return {
          coinDetails: response,
        };
      },
    }),
  }),
});

export const { useGetCoinDetailsQuery } = coingeckoApi;
