import { configureStore } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import EncryptedStorage from 'react-native-encrypted-storage';
import { rootReducer } from './rootReducer';
import { persistReducer, persistStore } from 'redux-persist';
import { baseApi } from './baseApi';
import { setupListeners } from '@reduxjs/toolkit/query';
import { coingeckoBaseApi } from './coingeckoBaseApi';
import { etherscanSepoliaBaseApi } from './etherscanSepoliaBaseApi';

const persistConfig = {
  key: 'root',
  storage: EncryptedStorage,
  // blacklist: ['wallet', 'user'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleWare => {
    const middlewares = getDefaultMiddleWare({
      serializableCheck: false,
    }).concat([baseApi.middleware, coingeckoBaseApi.middleware, etherscanSepoliaBaseApi.middleware]);
    if (__DEV__ && !process.env.JEST_WORKER_ID) {
      const createDebugger = require('redux-flipper').default;
      middlewares.push(createDebugger());
    }
    return middlewares;
  },
});

export const persistor = persistStore(store);
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

setupListeners(store.dispatch);
