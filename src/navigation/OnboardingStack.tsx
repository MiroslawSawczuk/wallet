import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ScreenName } from '../@types/navigation';
import { AddExistingWallet } from '../screens/AddExistingWallet/AddExistingWallet';
import { Welcome } from '../screens/Welcome/Welcome';
import { SelectNetwork } from '../screens/SelectNetwork/SelectNetwork';
import { RestoreWallet } from '../screens/RestoreWallet/RestoreWallet';
import { CreatePin } from '../screens/CreatePin/CreatePin';
import { Backup } from '../screens/Backup/Backup';
import { SecretPhrase } from '../screens/SecretPhrase/SecretPhrase';
import { SecretPhraseConfirm } from '../screens/SecretPhraseConfirm/SecretPhraseConfirm';

const Stack = createNativeStackNavigator();

export const OnboardingStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name={ScreenName.WELCOME} component={Welcome} />
      <Stack.Screen name={ScreenName.ADD_EXISTING_WALLET} component={AddExistingWallet} />
      <Stack.Screen name={ScreenName.SELECT_NETWORK} component={SelectNetwork} />
      <Stack.Screen name={ScreenName.RESTORE_WALLET} component={RestoreWallet} />
      <Stack.Screen name={ScreenName.CREATE_PIN} component={CreatePin} />
      <Stack.Screen name={ScreenName.BACKUP} component={Backup} />
      <Stack.Screen name={ScreenName.SECRET_PHRASE} component={SecretPhrase} />
      <Stack.Screen name={ScreenName.SECRET_PHRASE_REPEAT} component={SecretPhraseConfirm} />
    </Stack.Navigator>
  );
};
