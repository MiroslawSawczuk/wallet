import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StackName } from '../@types/navigation';
import { OnboardingStack } from './OnboardingStack';
import { HomeStack } from './HomeStack';
import { useAppSelector } from '../store/rootStore';
import { selectUser } from '../store/user/user.selectors';

const Stack = createNativeStackNavigator();

const MainNavigation = () => {
  const user = useAppSelector(selectUser);

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        {!user.isOnboarded ? (
          <Stack.Screen name={StackName.ONBOARDING_STACK} component={OnboardingStack} />
        ) : (
          <Stack.Screen name={StackName.HOME_STACK} component={HomeStack} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainNavigation;
