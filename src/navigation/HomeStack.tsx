import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ScreenName } from '../@types/navigation';
import { AssetOverview } from '../screens/AssetOverview/AssetOverview';
import { Portfolio } from '../screens/Portfolio/Portfolio';
import { Receive } from '../screens/Receive/Receive';
import { Send } from '../screens/Send/Send';

const Stack = createNativeStackNavigator();

export const HomeStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name={ScreenName.PORTFOLIO} component={Portfolio} />
      <Stack.Screen name={ScreenName.ASSET_OVERVIEW} component={AssetOverview} />
      <Stack.Screen name={ScreenName.SEND} component={Send} />
      <Stack.Screen name={ScreenName.RECEIVE} component={Receive} />
    </Stack.Navigator>
  );
};
